#!/bin/bash

function install_gitlab_runner() {
  # 打印开始
  echo "不存在gitrunner 即将开始安装"
  # 下载gitlab文件
  echo "下载添加yum源文件"
  curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bashc
  # 更新软件版本
  echo "----开始更新软件版本----"
  yum update -y
  # 下载 gitlab-runner
  echo "----开始安装文件----"
  yum install gitlab-runner -y
  # 启动gitlab-runner
  echo "----gitlab-runner 启动中----"
  systemctl start gitlab-runner
  # 设置开机自启动
  echo "----设置gitlab-runner开机启动----"
  systemctl enable gitlab-runner
}

function have_gitlab_runner() {
  echo "存在gitlab-runner 无需安装"
  gitlab_info=(${version// /})
  for var in ${gitlab_info[@]}; do
    echo $var
  done
}

version=$(gitlab-runner -v)
if [[ $? -eq 0 ]]; then
  have_gitlab_runner
else
  install_gitlab_runner
fi

systemctl start gitlab-runner


echo "----即将开始注册runner----"
# 设置 gitlab 地址
gitlab_url="https://git.fancyguo.com/"
readonly gitlab_url
# 设置项目token
gitlab_token="baUoq_QRRBSbqyjosuaY"
readonly gitlab_token
# 设置项目TAG
gitlab_tag='test'
# 域名私有证书
cert_file_path='/data/git.fancyguo.com.crt'
# 注册成 runner
register_bash="gitlab-runner register --non-interactive --url $gitlab_url --registration-token $gitlab_token --tag-list $gitlab_tag --description $gitlab_tag  --executor shell"
# 判断有没有使用证书
if [[ ${#cert_file_path} > 0 ]]; then
  register_bash="$register_bash --tls-ca-file $cert_file_path"
fi
# echo $register_bash
${register_bash}
